# pywand

[![PyPI version](https://badge.fury.io/py/pywand.svg)](https://badge.fury.io/py/pywand)

This library was written to help identify MagiQuest Wands.

Config is done via an ini file.

Output is typically in the form of a generator.

I'm working on more documentation.

Also, thanks to lots of kind people sharing their work on the internet. A future revision will include links.

## Installation, Setup, and Config

#### tldr:

- Enable your infrared device
- Install python3
- `pip install pywand`
- Edit `/etc/wand_config.ini`


#### More details available in the wiki:

- [Installation and Pre-Reqs](https://gitlab.com/rveach/wand/-/wikis/Installation-and-Pre-Reqs)
- [Configuration](https://gitlab.com/rveach/wand/-/wikis/pywand-Configuration)
